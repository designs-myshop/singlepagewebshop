
# Single Page Webshop
----

## Table of contents
---
- [Prerequisites](#Prerequisites)

## Prerequisites
----

##### 1. Install node.js
To check if you have Node.js installed, run this command in your terminal:
```
node -v
```
Otherwise download node.js at the following link: [nodejs.org](https://nodejs.org/en/).

<br>

##### 2. Install npm
To confirm that you have npm installed you can run this command in your terminal:
```
npm -v
```
Otherwise run the following command to install npm:
```
npm install npm@latest -g
```

<br>

##### 3. Install gulp
```
npm install --global gulp-cli
```
If you have any problems, check: [gulpjs.com](https://gulpjs.com/docs/en/getting-started/quick-start).

<br>
